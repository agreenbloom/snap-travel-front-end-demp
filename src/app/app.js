import angular from 'angular';
import ComponentRouter from '@angular/router/angular1/angular_1_router';
import UISelect from 'angular-ui-select/select.min';
import ngSanitize from 'angular-sanitize';
import Header from 'components/header/header.component';
import SearchForm from 'components/searchForm/searchForm.component';
import HotelCard from 'components/hotelCard/hotelCard.component';
import Filters from 'components/filters/filters.component.js';
import ServiceFactory from './app.utils';

import '../style/app.scss';
import 'angular-ui-select/select.min.css';

let AppComponent = {
  template: `
        <div class="container">
          <header></header>
          <search-form></search-form>
        </div>
    `,
  controller: () => new AppController()
};

class AppController { }

angular
  .module('synopsis', ['ngComponentRouter', 'ui.select', 'ngSanitize'])
  .config(($locationProvider) => $locationProvider.html5Mode(true))
  .value('$routerRootComponent', 'app')
  .component('app', AppComponent)
  .component('header', Header)
  .component('searchForm', SearchForm)
  .component('hotelCard', HotelCard)
  .component('filter', Filters);

angular
  .element(document)
  .ready(() => angular.bootstrap(document, ['synopsis']));

export default AppController;
