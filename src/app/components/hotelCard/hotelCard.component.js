import template from './hotelCard.html';
import styles from './hotelCard.scss';

export default {
    bindings: {
        hotel: '<',
    },

    template,

        controller: class {

        constructor($http) {
            'ngInject';
            this.styles = styles;

        }

        $onInit() {
            this.savings = Math.floor(this.hotel.price - this.hotel.snapPrice);
        }

        getStars() {
            return new Array(this.hotel.num_stars);
        }
    }
};
