import _ from 'lodash';

import template from './searchForm.html';
import styles from './searchForm.scss';

export default {

    template,

        controller: class {

        constructor($http) {
            'ngInject';
            this.styles = styles;
            this.$http = $http;
        }

        $onInit() {
            this.formData = {};
            this.results = [];
        }

        clearSearchTerm() {
            this.searchCity = '';
        }

        getSearchResults() {
            const { city, checkin, checkout } = this.formData;

            this.$http.post('https://experimentation.getsnaptravel.com/interview/hotels', {
                city,
                checkin,
                checkout,
                provider : 'snaptravel'
            }).then((resp) => {
                const snapTravelResults = resp.data.hotels.sort((a, b) => a.hotel_name !== b.hotel_name ? a.hotel_name < b.hotel_name  ? -1 : 1 : 0);

                this.$http.post('https://experimentation.getsnaptravel.com/interview/hotels', {
                    city,
                    checkin,
                    checkout,
                    provider : 'retail'
                }).then((data) => {

                    const hotelsDotComResults = data.data.hotels.sort((a, b) => a.hotel_name !== b.hotel_name ? a.hotel_name < b.hotel_name  ? -1 : 1 : 0);

                    hotelsDotComResults.map(hotel => {
                        snapTravelResults.map(snapHotel => {

                            if( hotel.hotel_name === snapHotel.hotel_name && snapHotel.price < hotel.price) {

                                const newHotel = hotel;
                                hotel.dotComPrice =  hotel.price;
                                hotel.snapPrice = snapHotel.price;

                                this.results = this.results.concat(newHotel)

                            }
                        });
                    });
                });
            });
        }
    }
};
