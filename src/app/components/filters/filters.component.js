import _ from 'lodash';

import template from './filters.html';
import styles from './filters.scss';

export default {
    bindings: {
        results: '<',
    },

    template,

        controller: class {

        constructor($http) {
            'ngInject';
            this.styles = styles;
        }

        $onInit() {
            if (this.results) console.log('hotels', this.results);
        }

        sortByPrice() {
            this.results = this.results.sort((a, b) => a.snapPrice !== b.snapPrice ? a.snapPrice < b.snapPrice  ? -1 : 1 : 0);
        }

        sortBySavings() {
            console.log('test');
            this.results.map(hotel => {
                hotel.savings =  Math.floor(hotel.price - hotel.snapPrice);
                return hotel;
            })

            this.results = this.results.sort((a, b) => a.savings !== b.savings ? a.savings > b.savings  ? -1 : 1 : 0);

        }

        sortByRating() {
            this.results = this.results.sort((a, b) => a.num_stars !== b.num_stars ? a.num_stars < b.num_stars  ? -1 : 1 : 0);
        }
    }
};
