import styles from './header.scss';
export default {
    template: `
        <section class='page-header-container'>
            <div class="logo">
                <img src="../../img/logo.png" alt="">
            </div>
            <p>SnapTravel</p>
        </section>
    `,
    controller: class {

        constructor($http) {
            'ngInject';
            this.styles = styles;
        }
    }
};
